import * as React from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { DateField } from "@mui/x-date-pickers/DateField";
import ResponsiveDatePickers from "./ResponsiveDatePickers";

export default function DatePickersComp() {
  return (
    <div>
      <h2>DatePicker</h2>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        Date Picker
        <DatePicker />
        Date Field
        <DateField />
      </LocalizationProvider>
      <ResponsiveDatePickers/>
    </div>
  );
}