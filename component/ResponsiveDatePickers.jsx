import * as React from "react";
import dayjs from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { MobileDatePicker } from "@mui/x-date-pickers/MobileDatePicker";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";

export default function ResponsiveDatePickers() {
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <h2>Desktop variant</h2>
      <DesktopDatePicker defaultValue={dayjs("2022-04-17")} />
      <div className="my-2">
        <h2>Mobile variant</h2>
        <MobileDatePicker defaultValue={dayjs("2022-04-17")} />
      </div>
      <h2>Responsive variant</h2>
      <DatePicker defaultValue={dayjs("2022-04-17")} />
    </LocalizationProvider>
  );
}
