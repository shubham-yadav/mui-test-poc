import React from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

export default function MaterialDatePickerPro(props) {
  let { date, handleDate, name, error } = props;
  return (
    <div>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DatePicker
          value={date}
          mask="__/__/____"
          format="DD/MM/YYYY"
          onChange={(newValue) => {
            handleDate(name, newValue);
          }}
          className="muiDatePicker border rounded"
        />
      </LocalizationProvider>
      {error && <small className="text-danger">{error}</small>}
    </div>
  );
}
