import React from "react";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { createTheme } from "@mui/material/styles";
import { ThemeProvider } from "@mui/material/styles";

export default function MaterialDatePicker(props) {
  let { date, handleDate, name, error, chatDatePicker = false } = props;
  const newTheme = (theme) =>
    createTheme({
      ...theme,
      components: {
        MuiPickersCalendarHeader: chatDatePicker
          ? {
              styleOverrides: {
                label: {
                  position: "fixed",
                  top: 20,
                },
                switchViewButton: {
                  width: "100%",
                  marginLeft: "25px",
                },
                labelContainer: {
                  width: "100%",
                },
              },
            }
          : {},
      },
    });
  return (
    <div>
      <ThemeProvider theme={newTheme}>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DatePicker
            value={date}
            mask="__/__/____"
            format="DD/MM/YYYY"
            onChange={(newValue) => {
              handleDate(name, newValue);
            }}
            className="muiDatePicker border rounded"
          />
        </LocalizationProvider>
      </ThemeProvider>
      {error && <small className="text-danger">{error}</small>}
    </div>
  );
}
