import Modal from "react-bootstrap/Modal";

import React from "react";
import DatePickersComp from "./DatePickersComp";
import dynamic from "next/dynamic";
import MaterialDatePicker from "./MaterialDatePicker";
import MaterialDatePickerPro from "./MaterialDatePickerPro";
const MaterialDatePickerD = dynamic(
  () => import("@/component/MaterialDatePicker"),
  {
    ssr: false,
  }
);
const MaterialDatePickerProD = dynamic(
  () => import("@/component/MaterialDatePickerPro"),
  {
    ssr: false,
  }
);
export default function BootstrapModal(props) {
  const { show, setShow } = props;
  return (
    <Modal
      show={show}
      onHide={() => setShow(false)}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
      //   className="kundaliModal"
    >
      <Modal.Body className="kundaliFormCont">
        <h2>Boot strap Modal</h2>
        <DatePickersComp />
          Project Picker D
          <MaterialDatePickerD />
          Project Picker
          <MaterialDatePicker />
          Project Pro Picker D
          <MaterialDatePickerProD />
          Project Pro Picker
          <MaterialDatePickerPro />
      </Modal.Body>
    </Modal>
  );
}
